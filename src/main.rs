#![deny(warnings)]

use serde::{Deserialize, Serialize};

use hyper::service::{make_service_fn, service_fn};
use hyper::{Body, Method, Request, Response, Server, StatusCode};
use failure::Error;

// use std::io::Cursor;

// use mqtt::{Encodable, Decodable};
use mqtt::packet::{ PublishPacket, QoSWithPacketIdentifier};
use mqtt::TopicName;

#[derive(Serialize, Deserialize)]
struct DataRequest {
    datastreams: Vec<DataStream>
}

#[derive(Serialize, Deserialize)]
struct DataStream {
    id: String,
    datapoints: Vec<DataPoint>
}

#[derive(Serialize, Deserialize)]
struct DataPoint {
    value: String
}

/// This is our service handler. It receives a Request, routes on its
/// path, and returns a Future of a Response.
async fn handler(req: Request<Body>) -> Result<Response<Body>, Error> {
    match (req.method(), req.uri().path()) {
        (&Method::GET, "/") => Ok(Response::new(Body::from(
            "Hello.",
        ))),

        (&Method::POST, "/register_de") => Ok(Response::new(Body::from(
            "{\"errno\":0,\"data\":{\"device_id\":\"593821791\",\"key\":\"8AoA====pTihvZUskdHmvAjg0cA=\"},\"error\":\"succ\"}",
        ))),

        (&Method::POST, "/devices/593821791/datapoints") => {
            let whole_body = hyper::body::to_bytes(req.into_body()).await?;
            let v:DataRequest = match serde_json::from_slice(&whole_body) {
                Err(e) => {
                    return Ok(Response::new(Body::from(e.to_string())))
                },
                Ok(v) => v,
            };

            for idx in 0..v.datastreams.len() {
                let id = &v.datastreams[idx].id.to_ascii_lowercase();
                let value = &v.datastreams[idx].datapoints[0].value;

                let topic = format!("sensorbox2/{}",id);

                let _packet = PublishPacket::new(TopicName::new(topic).unwrap(),
                                QoSWithPacketIdentifier::Level2(10),
                                value.as_bytes());
            }

            Ok(Response::new(Body::from("")))
        }

        _ => {
            let mut not_found = Response::default();
            *not_found.status_mut() = StatusCode::NOT_FOUND;
            Ok(not_found)
        }
    }
}

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let addr = ([127, 0, 0, 1], 3000).into();

    let service = make_service_fn(|_| async { Ok::<_, hyper::Error>(service_fn(handler)) });

    let server = Server::bind(&addr).serve(service);

    println!("Listening on http://{}", addr);

    server.await?;

    Ok(())
}